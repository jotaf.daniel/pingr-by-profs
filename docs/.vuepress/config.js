module.exports = {
  themeConfig: {
    nav: [
      {text: "Pingr", link: "/"},
      {
        text: "Páginas",
        items: [
          {text: "Design", link: "/pages/design.md"},
          {text: "Home", link: "/pages/home.md"},
          {text: "Feed", link: "/pages/feed.md"},
          {text: "Compose", link: "/pages/compose.md"},
          {text: "Perfil", link: "/pages/profile.md"}
        ]
      },
      {
        text: "Padrões",
        ariaLabel: "Análises de Trade-Offs de Padrões",
        items: [
          {text: "API Gateway", link: "/extras/apigateway_tradeoffs.md"},
          {
            text: "Asynchronous Messaging",
            link: "/extras/asyncmsg_tradeoffs.md"
          },
          {text: "CQRS", link: "/extras/cqrs_tradeoffs.md"},
          {
            text: "Database per Service",
            link: "/extras/dbperservice_tradeoffs.md"
          },
          {text: "Event Sourcing", link: "/extras/eventsourcing_tradeoffs.md"}
        ]
      }
    ]
  }
};
