# Pingr

[[TOC]]

:::warning AVISO
Há uma atualização na documentação: os _wireframes_ das páginas estão disponíveis!
Acesse o menu na barra de navegação.
:::

## Contexto

O **Pingr** é um sistema hipotético inspirado no [Twitter][twitter]. Foi criado como projeto para o curso **Arquitetura Ágil de Software**, ministrado durante o [Programa de Verão - IME-USP - 2021][veraoimeusp].

Agora será utilizado com projeto na disciplina [MAC0475 - Laboratório de Sistemas Computacionais Complexos][complexos-ementa] oferecida pelo Departamento de Ciência da Computação - IME-USP.

Todo o material de documentação do sistema estará disponível nesta página.

## Introdução

O Pingr é uma rede social no formato de microblog, caracterizada pelo fato de que os usuários podem postar atualizações limitadas a 140 caracteres. Cada atualização, chamada de ping, pode ser respondida em um novo ping, pode receber uma curtida ou então pode receber pongs -- um compartilhamento por outros usuários.

Depois de um sucesso estrondoso nos primeiros dois anos e do constante crescimento da base de usuários, a empresa Pingr recebeu um grande aporte financeiro na virada de 2020 para 2021. Esse aporte possibilitou a mudança de escritório para um novo lugar onde poderão fazer as rodadas de contratação que planejavam. O planejamento é multiplicar a equipe de desenvolvimento -- indo de 15 para 250 membros ao longo dos próximos 12 meses.

Consulte os requisitos do sistema [aqui][requisitospingr].

## Proposta de Arquitetura

### Panorama

::: warning IMPORTANTE
Esta proposta é de autoria dos professores do curso. Seu propósito é servir como referência do processo de criação, da adoção de padrões e da condução do raciocínio durante a documentação. **Não é a única arquitetura correta**.
:::

Abaixo, uma visão de microsserviços panorâmica.

![visão de microsserviços base][msviewbase]

As resposnsabilidades e funcionalidades de cada microsserviço são as seguintes:

1. **Autenticação**
  - realiza a autenticação das requisições HTTP(S) que entram no sistema. A _autorização_ deve ser realizada por cada um dos demais microsserviços;
  - time: **Squirtle**
  - repositório: 

2. **Bate Papo**
  - gerencia o conteúdo privativo, ou _Direct Pings (DPs)_, permitindo criação de novas conversas e leitura e envio de novas mensagens;
  - time: **Bulbasaur**
  - repositório: 

3. **Recomendação**
  - faz a indexação de todo o conteúdo de pings para gerar as listas de _TagNow_ (_Here_ e _World_), e dispara buscas por _hashtags_;
  - time: **Abra**
  - repositório: 

4. **Conexões**
  - gerencia a relação entre usuários, permitindo a solicitação e resolução de solicitação de amizade, além de visualizar e gerenciar a lista de amigos especiais;
  - time: **Pikachu**
  - repositório: 

5. **Feed**
  - gerencia o conteúdo exibido a um usuário em suas mesas, e também permite a criação de mesas secundárias;
  - time: **Sandshrew**
  - repositório: 

6. **Interação**
  - gerencia a interação em pings, permitindo curtir e _pongar_ (compartilhar);
  - time: **Charmander**
  - repositório: 

7. **Ping**
  - permite a criação e a remoção de pings, bem como a edição de visibilidade de um ping (público, privado ou restrito aos amigos especiais);
  - time: **Ekans**
  - repositório: 

8. **Perfil**
  - gerencia a entidade Usuário, permitindo criação, atualização e remoção de conta, além de gerenciar a visibilidade de cada usuário (público ou privado);
  - time: **Onix**
  - repositório: 

::: tip OBSERVAÇÃO
Todos os serviços fazem uso de bancos de dados para persistência e busca de informações, e cada serviço poderia se beneficiar de um modelo de dados diferente. Porém, por questões didáticas, todos os serviços utilizarão o [MongoDB][mongodb].
:::

### Eventos e Comunicação interna

Abaixo, uma visão de eventos e comunicação entre serviços.

![visão de eventos - base][eventsview-base]


## Mapeamento Time-Serviço

1. Perfil - Onix
2. Autenticação - Squirtle
3. Conexões - Pikachu
4. Bate-Papo - Bulbasaur
5. Ping - Ekans
6. Recomendação - Abra
7. Interação - Charmander
8. Feed - Sandshrew


## Padrões

Nesta proposta de arquitetura, alguns padrões de microsserviço são adotados. São eles:

  - [API Gateway][apigateway_tradeoffs]
  - [Asynchronous Messaging][asyncmsg_tradeoffs]
  - [CQRS][cqrs_tradeoffs]
  - [Database per Service][dbpersvc_tradeoffs]
  - [Event Sourcing][eventsourcing_tradeoffs]


[twitter]: https://twitter.com
[veraoimeusp]: https://www.ime.usp.br/~verao/index.php
[complexos-ementa]: https://uspdigital.usp.br/jupiterweb/obterDisciplina?nomdis=&sgldis=mac0475
[requisitospingr]: ./extras/requisitos.md
[msviewbase]: ./msview-base.png
[mongodb]: https://www.mongodb.com/

[eventsview-base]: ./eventsview-base.png

[cqrs_tradeoffs]: ./extras/cqrs_tradeoffs.md
[apigateway_tradeoffs]: ./extras/apigateway_tradeoffs.md
[eventsourcing_tradeoffs]: ./extras/eventsourcing_tradeoffs.md
[asyncmsg_tradeoffs]: ./extras/asyncmsg_tradeoffs.md
[dbpersvc_tradeoffs]: ./extras/dbperservice_tradeoffs.md
