# Definições de Design


## Paleta de cores

![paleta][colors]


## Exemplo de visualização

![exemplo][sample]


[colors]: ./colors.png
[sample]: ./sample.png
